package clients;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class ReportClient {

    static String port = "5007/";

    private static String getRootPath(){
        String env = System.getenv("RUNNING_ENV");
        if (env == null) return "http://localhost:";
        else return "http://reportgeneration:";
    }

    static WebTarget baseUrl = ClientBuilder.newClient().target(getRootPath()+ port);
    public Response getManagerReport(){
        return baseUrl
                .path("report")
                .path("manager")
                .request()
                .get();
    }


}