package clients;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import dtu.ws.fastmoney.User;

public class AccountManagementClient {
    WebTarget baseUrl;


    private static String getRootPath(){
        String env = System.getenv("RUNNING_ENV");
        if (env == null) return "http://localhost:";
        else return "http://accountmanagement:";
    }

    public AccountManagementClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target(getRootPath() + "5002/");
    }

    public Response changeAccountInformation(String id, User user){
        return baseUrl
                .path("accounts")
                .path(id)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .put(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    public Response retrieveCustomerAccount(String cid){
        return baseUrl
                .path("accounts")
                .path("customer")
                .path(cid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response retrieveMerchantAccount(String mid){
        return baseUrl
                .path("accounts")
                .path("merchant")
                .path(mid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response retireAccount(String id) {
        return baseUrl
                .path("accounts")
                .path(id)
                .request()
                .delete();
    }
}
