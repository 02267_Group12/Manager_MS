package org.acme;

import clients.AccountManagementClient;
import clients.ReportClient;
import dtu.rs.entities.CustomerAccount;
import dtu.rs.entities.DTUPayAccount;
import dtu.rs.entities.MerchantAccount;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.Transaction;
import dtu.ws.fastmoney.User;
import entity.local.DTUTransaction;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/manager")
public class ManagerResource {

    AccountManagementClient accountManagementClient = new AccountManagementClient();
    ReportClient reportClient = new ReportClient();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/customer/{cid}")
    public Response getCustomerAccount(@PathParam("cid") String cid){
        Response response = accountManagementClient.retrieveCustomerAccount(cid);
        if (response.getStatus() == 200){
            DTUPayAccount customer = response.readEntity(DTUPayAccount.class);
            return Response
                    .ok(new GenericEntity<DTUPayAccount>(customer){})
                    .build();
        }else{
            return Response
                    .status(response.getStatus(), "Failed to get customer with cid: " + cid)
                    .entity("Failed to get customer with cid: " + cid)
                    .build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/customer/{cid}")
    public Response updateCustomerAccount(@PathParam("cid") String cid, User customer){
        Response response = accountManagementClient.changeAccountInformation(cid, customer);
        if (response.getStatus() == 200){
            return Response
                    .ok()
                    .build();
        } else {
            return Response
                    .status(response.getStatus(), "Failed to update customer with cid: " + cid)
                    .entity("Failed to update customer with cid: " + cid)
                    .build();
        }
    }

    @DELETE
    @Path("/customer/{cid}")
    public Response deleteCustomerAccount(@PathParam("cid") String cid){
        Response response = accountManagementClient.retireAccount(cid);
        if (response.getStatus() == 200){
            return Response
                    .ok()
                    .build();
        } else {
            return Response
                    .status(response.getStatus(), "Failed to delete customer with cid: " + cid)
                    .entity("Failed to delete customer with cid: " + cid)
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/merchant/{mid}")
    public Response getMerchantAccount(@PathParam("mid") String mid){
        Response response = accountManagementClient.retrieveMerchantAccount(mid);
        if (response.getStatus() == 200){
            DTUPayAccount merchant = response.readEntity(DTUPayAccount.class);
            return Response
                    .ok(new GenericEntity<DTUPayAccount>(merchant){})
                    .build();
        }else{
            return Response
                    .status(response.getStatus(), "Failed to get merchant with cid: " + mid)
                    .entity("Failed to get merchant with cid: " + mid)
                    .build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/merchant/{mid}")
    public Response updateMerchantAccount(@PathParam("mid") String mid, User merchant){
        Response response = accountManagementClient.changeAccountInformation(mid, merchant);
        if (response.getStatus() == 200){
            return Response
                    .ok()
                    .build();
        } else {
            return Response
                    .status(response.getStatus(), "Failed to update customer with cid: " + mid)
                    .entity("Failed to update customer with cid: " + mid)
                    .build();
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/merchant/{mid}")
    public Response deleteCostumerAccount(@PathParam("mid") String mid){
        Response response = accountManagementClient.retireAccount(mid);
        if (response.getStatus() == 200){
            return Response
                    .ok()
                    .build();
        } else {
            return Response
                    .status(response.getStatus(), "Failed to delete customer with cid: " + mid)
                    .entity("Failed to delete customer with cid: " + mid)
                    .build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/report")
    public Response getReport(){
        Response response = reportClient.getManagerReport();
        if (response.getStatus() == 200){
            // List<DTUTransaction> transactions = response.readEntity(new GenericType<List<DTUTransaction>>(){});
            return response;
        } else {
            return Response
                    .status(response.getStatus(), "Failed to get manager report")
                    .entity("Failed to get manager report")
                    .build();
        }
    }
}
