package dtu.rs.entities;

public class InvalidUseException extends Exception
{
    public InvalidUseException(String message)
    {
        super(message);
    }

}